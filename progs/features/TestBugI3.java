class TestBugI3 {
    public static void main(String[] a) {
        System.out.println(new Test().f());
    }
}

class Test {

    public int f() {

        int result;
        int x; 
        result = 2;
        x = 0;
        x++;
        
        if (!x) {
            result = 0;
        }
        else {
            result = 1;
        }
        
        return result;
    }
}